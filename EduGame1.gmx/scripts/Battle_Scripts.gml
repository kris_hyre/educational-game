#define Battle_Scripts
/* This collection will include scripts relevant to combat 
 * between the player and enemy objects
 */

#define scr_format_health
/// @args health
var current_health = argument0;
var display_health = ""

if (current_health >= 100){
    display_health = string(current_health);    
} else if (current_health > 9 && current_health < 100){
    display_health = "0" + string(current_health);
} else if (current_health <= 9 && current_health >= 0){
    display_health = "00" + string(current_health);
} else if (current_health < 0){
    display_health = "000";
}

return display_health;
#define scr_parse_digit_entry
/// @args player_entry
// This script scrubs the entry to ensure only digits

// Array of 0-9
var digits;
for (var i = 0; i < 10; i++){
    digits[i] = i;    
}
var digits_length = array_length_1d(digits);


var current_entry = argument0;
var valid_entry = "";
var max_length = 9; 
var entry = keyboard_string;
keyboard_string = "";

// Check for digit entry
if (string_length(current_entry) < 9){
    for (var j = 0; j < digits_length; j++){
        if (string(digits[j]) == entry){
            valid_entry = valid_entry + entry;
        }
    }
}

return valid_entry;
#define scr_randomize_operation
/* This script is used to randomly determin the type of operation
 * to be used with the battle
 */
 
var choice = irandom_range(0, 1); // TODO: currently set to only return 0 or 1

switch (choice){
    case 0:
        return operation.add;
    case 1:
        return operation.subtract;
    case 2:
        return operation.multiply;
    case 4: 
        return operation.divide;
    default:
        return operation.add;
}

#define scr_random_operand
// Returns an operand depending on the difficulty level

if (global.game_difficulty == 1){
    var value = irandom_range(0, 50);
    return value;
}