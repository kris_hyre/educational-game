#define Button_Scripts
/* is is a collection of button scripts.

Buttons are designed on left-click release to execute a script based on an "action" property of each button.
This is the collection of those scripts.

By default, children of the obj_Button parent should be using scr_button_default as
their action variable, which just displayed a "NYI" message. 

Could probably implement some of these as code blocks with the appropriate object
instead of scripts as they are not reused. Keeping them here though allows for just
updating the action value of the button with the new script name if we need to change 
functionality.

*/

#define scr_button_default
///scr_button_default()
var nyi = "This function has not yet been implemented.";
show_message(nyi);

#define scr_end_game
// Exit the game

// TODO: Add memory clean up and data save checks here

game_end();


#define scr_start_game
// Game start button
// Had issues with passing an arguement so hard coded it into the script, need to investigate that issue

var target_room = rm_game_start;
room_goto(target_room);
#define scr_solution
solution_submitted = true;