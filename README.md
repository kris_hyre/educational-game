This repo is for an educational game project currently under development using GML 1.4. I am currently targeting Windows, and Android platforms, with potential for Web and iOS.
The gameplay will be light RPG mechanics with combat determined by randomized spelling and math challenges depending on the user skill level.

krishyre@gmail.com